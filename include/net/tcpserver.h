﻿#ifndef QTSOCKET_TCPSERVER_H
#define QTSOCKET_TCPSERVER_H

#include "qnet_global.h"
#include <QTcpServer>

class QTcpSocket;

QNET_NAMESPACE_BEGIN
class TcpSession;
class ISessionListener;

/**
 * @brief The TcpServer class 表示tcp服务端。
 */
class QNETSHARED_EXPORT TcpServer : public QTcpServer
{
    Q_OBJECT
public:
    explicit TcpServer(ISessionListener *listener, QObject *parent = nullptr);
    virtual ~TcpServer() override;
    /**
     * @brief disconnect 移除连接。
     * @param session
     */
    void disconnect(TcpSession *session);
    /**
     * @brief sessionList 获取会话列表。
     * @return
     */
    QList<TcpSession *> sessionList() const;
    /**
     * @brief setListener 设置回调。
     * @param listener 回调的实例，可为空。
     */
    void setListener(ISessionListener *listener);
private:
    void seesionRemove(TcpSession *session);
    /**
     * @brief onChange 状态改变处理。
     * @param session
     * @param state
     */
    void onChange(TcpSession *session, bool state);
public slots:
    /**
     * @brief incomingConnection 新连接通知。
     * @param socketDescriptor socket 句柄。
     */
    void incomingConnection(qintptr socketDescriptor) override;
    /**
     * @brief disconnectedSlot 断开连接则从列表中删除 并释放
     */
    void disconnectedSlot();

    /**
     * @brief stateChanged 状态改变。
     * @param state
     */
    void stateChanged(QAbstractSocket::SocketState state);
    /**
     * @brief error socket异常。
     * @param socketError
     */
    void error(QAbstractSocket::SocketError socketError);
private:
    QList<TcpSession *> m_sessionList;
    ISessionListener *m_listener; // 监听器实例。
};
QNET_NAMESPACE_END
#endif // TCPSERVER_H
