﻿#ifndef LISTENER_H
#define LISTENER_H
#include "qnet_global.h"

#include <QAbstractSocket>
QNET_NAMESPACE_BEGIN
class TcpSession;
/**
 * @brief The ISessionListener class 会话监视接口。
 */
class QNETSHARED_EXPORT ISessionListener
{
public:
    virtual ~ISessionListener();
    /**
     * @brief onStateChanged 会话状态改变。
     * @param session 会话。
     * @param connected 状态是否链接，false表示断开。
     */
    virtual void onStateChanged(QtSocket::TcpSession *session, bool connected) = 0;
    /**
     * @brief onError 错误信息。
     */
    virtual void onError(QAbstractSocket::SocketError) = 0 ;
};
QNET_NAMESPACE_END
#endif // LISTENER_H
