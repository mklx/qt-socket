﻿#ifndef QTSOCKET_NETWRITING_H
#define QTSOCKET_NETWRITING_H

#include <QObject>
#include "qnet_global.h"

QNET_NAMESPACE_BEGIN
/**
 * @brief The NetWriting class 表示网络数据传输写入类。
 */
class QNETSHARED_EXPORT NetWriting : public QObject
{
    Q_OBJECT
public:
    explicit NetWriting(QObject *parent = nullptr);
    NetWriting(const QByteArray &array, QObject *parent = Q_NULLPTR);

    QByteArray data() const;
    void setData(const QByteArray &data);

    int size() const;
signals:
    /**
       * @brief fulfil 写入完成信号。
       * @param size 写入的数据长度。
       */
    void fulfil(QSharedPointer<NetWriting> sender, int size);
private:
    QByteArray m_data;
};
QNET_NAMESPACE_END

#endif // NETWORKWRITING_H
