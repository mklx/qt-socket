﻿#include <net/netwriting.h>
#include <QtDebug>

QNET_USING_NAMESPACE

NetWriting::NetWriting(QObject *parent) : QObject(parent)
{

}

NetWriting::NetWriting(const QByteArray &array, QObject *parent)
    :QObject (parent)
{
    m_data = array;
}

QByteArray NetWriting::data() const
{
    return m_data;
}

void NetWriting::setData(const QByteArray &data)
{
    m_data = data;
}

int NetWriting::size() const
{
    return m_data.size();
}
