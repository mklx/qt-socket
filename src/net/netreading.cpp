﻿#include <net/netreading.h>

QNET_USING_NAMESPACE

NetReading::NetReading()
{

}

NetReading::NetReading(const NetReading &src)
{
    m_host = src.host();
    m_port = src.port();
}

NetReading::~NetReading()
{

}

QHostAddress NetReading::host() const
{
    return m_host;
}

void NetReading::setHost(const QHostAddress &host)
{
    m_host = host;
}

quint16 NetReading::port() const
{
    return m_port;
}

void NetReading::setPort(const quint16 &port)
{
    m_port = port;
}
