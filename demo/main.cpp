﻿#include "tcpclientdemo.h"
#include "tcpserverdemo.h"
#include "udpchanneldemo.h"

#include <QCoreApplication>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QString type = "u";
    if(argc > 1)
    {
        type = argv[1];
    }
    TcpClientDemo clientDemo;
    TcpServerDemo serverDemo;
    UdpChannelDemo udpDemo;
    bool initOk = false;
    QHostAddress local("127.0.0.1");
    if(type == "c") // client
    {
         clientDemo.connect(local, 6666);
         initOk = true;
    }else if(type == "s") // server
    {
        initOk = serverDemo.start(local, 6666);
    }else if(type == "u")  // udp
    {
        initOk = udpDemo.start(local, 6666, local, 6667);
    }

    if(initOk == false)
    {
        qDebug() << QObject::tr("Failed to init!");
    }
    return a.exec();
}
